// settings
const path = require("path");
const rootPath = path.join(__dirname, "/../..");
const staticPath = path.join(__dirname, "/../../client/public/");

const settings = {
  titleProject: "..",
  rootPath: rootPath,
  staticPath: staticPath,
  envPath: __dirname,
  port: 5139,
};

module.exports = settings;
