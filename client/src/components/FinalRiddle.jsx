import React from "react";
import { Fab, Grow, Collapse } from "@mui/material";
import DownloadIcon from "@mui/icons-material/Download";

function FinalRiddle(props) {
  const { width, isOpen, highlightColor } = props;
  return (
    <React.Fragment>
      <Collapse
        in={isOpen}
        sx={{
          mt: 2,
          width: `${width + 14}px`,
        }}
      >
        <Grow
          in={isOpen}
          style={{ transformOrigin: "0 0 0" }}
          {...(isOpen ? { timeout: 1000 } : {})}
        >
          <Fab
            sx={{ background: highlightColor, width: `${width + 14}px` }}
            variant="extended"
            href="https://leonardo-da-sharks-storage.s3.eu-central-1.amazonaws.com/12345678910.mp3"
          >
            {isOpen ? (
              <React.Fragment>
                <DownloadIcon />
                Download music
              </React.Fragment>
            ) : (
              <span style={{ height: "56px" }} />
            )}
          </Fab>
        </Grow>
      </Collapse>
    </React.Fragment>
  );
}

export default FinalRiddle;
