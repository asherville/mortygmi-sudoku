import React, { useEffect, useState } from "react";
import axios from "axios";

import { ThemeProvider, createTheme } from "@mui/material/styles";
import { Paper, Grid, Fade } from "@mui/material";

import CustomInput from "./components/CustomInput";

function Sudoku(props) {
  const { width, borderColor, highlightColor, onCorrectAnswer } = props;

  const { customInputStyle, paperStyle, singleInputStyle } = getCustomStyles(
    borderColor,
    highlightColor,
    width
  );

  const [data, setData] = useState();
  const [usersAnswer, setUsersAnswer] = useState();
  const [isCorrectAnswer, setCorrectAnswer] = useState(false);
  const [visible, setVisible] = useState(false);

  const inputRefs = Array.from(Array(9 * 9 + 1)).map((_, index) =>
    React.createRef()
  );
  useEffect(async () => {
    try {
      const response = await axios.get(
        "http://localhost:5139/getSudokuData",
        {}
      );
      if (response.status === 200) {
        const {
          initialAnswer,
          metaObj,
          resultedAnswer,
          afterChange,
          path,
          fullPath,
        } = response.data;
        setData({
          metaObj,
          initialAnswer,
          resultedAnswer,
          afterChange,
          path,
          fullPath,
        });
      }
    } catch (err) {
      console.log(err, "err");
    }
  }, []);

  const onFieldChange = (e, id) => {
    e.preventDefault();
    let newUsersAnswer = usersAnswer;
    if (!newUsersAnswer) {
      newUsersAnswer = data.initialAnswer;
    }
    const value = e.target.value || "";
    if (
      !value ||
      ["1", "2", "3", "4", "5", "6", "7", "8", "9"].indexOf(value) > -1
    ) {
      newUsersAnswer[id - 1] = value ? value.toString() : "";
      setUsersAnswer([...newUsersAnswer]);

      const currIndexPath = data.path.indexOf(id);
      const nextIndexRef = data.path[currIndexPath + 1];
      const next_input = nextIndexRef ? inputRefs[nextIndexRef] : null;
      if (value && next_input && next_input.current) {
        next_input.current.focus();
      }
      if (newUsersAnswer.indexOf("") === -1) {
        checkAnswer();
      }
    }
  };
  const checkAnswer = () => {
    let isCorrect = true;
    for (let i = 0; i < usersAnswer.length; i++) {
      if (usersAnswer[i].toString() !== data.resultedAnswer[i]) {
        isCorrect = false;
        break;
      }
    }
    if (isCorrect) {
      setCorrectAnswer(true);
      setUsersAnswer([...data.afterChange]);
      if (onCorrectAnswer) {
        onCorrectAnswer();
      }
    }
  };

  const renderSegment = (width, outer_index) => {
    if (!data) return;
    const isDeepRender = typeof outer_index === "number";
    return (
      <ThemeProvider theme={isDeepRender ? customInputStyle : {}}>
        <Grid
          container
          rowSpacing={0}
          sx={{ width: `${width}px`, height: `${width}px` }}
        >
          {Array.from(Array(3 * 3)).map((_, index) => (
            <Grid item xs={12 / 3} key={index}>
              {isDeepRender ? (
                <CustomInput {...getInputProps(outer_index * 9 + index + 1)} />
              ) : (
                renderSegment(width / 3, index)
              )}
            </Grid>
          ))}
        </Grid>
      </ThemeProvider>
    );
  };
  const getInputProps = (id) => {
    const { metaObj, afterChange } = data;

    const disabled = metaObj[id].disabled || isCorrectAnswer;
    let value;
    if (!isCorrectAnswer) {
      value = metaObj[id].disabled
        ? metaObj[id].value
        : usersAnswer
        ? usersAnswer[id - 1]
        : "";
    } else {
      value = afterChange[id - 1];
    }
    if (id === 81 && !visible) setVisible(true);
    return {
      id,
      value,
      disabled,
      onChange: (e) => onFieldChange(e, id),
      ...singleInputStyle,
      ref: inputRefs[id],
    };
  };
  return (
    <React.Fragment>
      <Fade in={visible}>
        <Paper sx={paperStyle}>{renderSegment(width)}</Paper>
      </Fade>
      >
    </React.Fragment>
  );
}

export default Sudoku;

const getCustomStyles = (borderColor, highlightColor, width) => {
  const customInputStyle = createTheme({
    components: {
      MuiTextField: {
        styleOverrides: {
          root: {
            height: "100%",
          },
        },
      },
      MuiOutlinedInput: {
        styleOverrides: {
          root: {
            height: "100%",
            borderRadius: 0,
          },
        },
      },
      MuiGrid: {
        styleOverrides: {
          root: {
            "&.MuiGrid-item": {
              maxHeight: `${100 / 3}%`,
              borderWidth: "1px",
              ".MuiOutlinedInput-input, .MuiInputBase-input, .css-24rejj-MuiInputBase-input-MuiOutlinedInput-input, .css-1d3z3hw-MuiOutlinedInput-notchedOutline, .MuiOutlinedInput-notchedOutline, .MuiInputBase":
                {
                  borderColor,
                  borderWidth: "1px",
                },
              "&:nth-of-type(n+7)": {
                ".css-1d3z3hw-MuiOutlinedInput-notchedOutline, .MuiOutlinedInput-notchedOutline, .MuiInputBase":
                  {
                    borderBottomWidth: "2px",
                    //background: "red",
                  },
              },
              "&:nth-of-type(-n + 3)": {
                ".css-1d3z3hw-MuiOutlinedInput-notchedOutline, .MuiOutlinedInput-notchedOutline, .MuiInputBase":
                  {
                    borderTopWidth: "2px",
                    //background: "green",
                  },
              },
              "&:nth-of-type(3n + 1)": {
                ".css-1d3z3hw-MuiOutlinedInput-notchedOutline, .MuiOutlinedInput-notchedOutline, .MuiInputBase":
                  {
                    borderLeftWidth: "2px",
                    //background: "yellow",
                  },
              },
              "&:nth-of-type(3n)": {
                ".css-1d3z3hw-MuiOutlinedInput-notchedOutline, .MuiOutlinedInput-notchedOutline, .MuiInputBase":
                  {
                    //background: "black",
                    borderRightWidth: "2px",
                  },
              },
            },
          },
        },
      },
    },
  });
  const paperStyle = {
    // bug with 1px mismatch I'm to lazy to fix normally
    border: `solid 7px ${borderColor}`,
    background: `${borderColor}`,
    borderRadius: "10px",
    zIndex: 3,
  };
  const singleInputStyle = {
    borderColor,
    highlightColor,
    fontSize: `${width / 9 - 20}px`,
  };
  return { customInputStyle, paperStyle, singleInputStyle };
};
