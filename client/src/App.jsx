import React, { useState, useEffect } from "react";
import { useWindowSize } from "utils/custom-hooks";

import FakeAuth from "components/FakeAuth";
import Sudoku from "components/Sudoku/Sudoku";
import FinalRiddle from "components/FinalRiddle";

function App() {
  const [isAuth, setIsAuth] = useState(false);
  const [sudokuWidth, setSudokuWidth] = useState(0);
  const [width, height] = useWindowSize();
  const [finalRiddle, setFinalRiddle] = useState(false);

  const audioEl = new Audio(
    "https://leonardo-da-sharks-storage.s3.eu-central-1.amazonaws.com/12345678910.mp3"
  );
  useEffect(() => {
    const min_side = Math.min(width, height) - 200;
    if (min_side > 0) {
      setSudokuWidth(Math.min(width, height) - 200);
    }
  }, [width, height]);

  const openFinalRiddle = () => {
    setFinalRiddle(true);
    if (audioEl.HAVE_ENOUGH_DATA === audioEl.readyState) {
      audioEl
        .play()
        .catch((err) => console.log("Problem with music playing occured", err));
    } else {
      audioEl.addEventListener("canplaythrough", () => {
        myAudioElement
          .play()
          .catch((err) =>
            console.log("Problem with music playing occured", err)
          );
      });
    }
  };
  return (
    <div>
      <div className="app-wrapper">
        {isAuth && sudokuWidth ? (
          <div>
            <Sudoku
              width={sudokuWidth}
              borderColor="#4eaacc"
              highlightColor={"#e762d7ff"}
              onCorrectAnswer={() => openFinalRiddle()}
            />
            <FinalRiddle
              isOpen={finalRiddle}
              width={sudokuWidth}
              highlightColor={"#e762d7ff"}
            />
          </div>
        ) : null}
        {!isAuth && <FakeAuth changeAuth={() => setIsAuth(true)} />}
      </div>
    </div>
  );
}

export default App;
