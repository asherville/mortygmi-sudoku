import { TextField } from "@mui/material";

import React from "react";

const CustomInput = React.forwardRef((props, ref) => {
  const { onChange, disabled, value, borderColor, highlightColor, fontSize } =
    props;
  const inputStyle = getStyle(borderColor, highlightColor, fontSize);

  return (
    <TextField
      sx={inputStyle}
      autoComplete="off"
      margin="none"
      fullWidth
      disabled={disabled}
      value={value || ""}
      onChange={!disabled ? onChange : null}
      inputProps={{
        ref,
      }}
    />
  );
});

export default CustomInput;

const getStyle = (borderColor, highlightColor, fontSize) => {
  return {
    zIndex: 4,
    background: "white",
    input: {
      textAlign: "center",
      fontSize,
      padding: 0,
    },
    // some cringe here, I'm tired
    ".MuiOutlinedInput, .MuiInputBase, .Mui-focused fieldset, .Mui-disabled fieldset":
      {
        borderColor: `${borderColor} !important`,
        borderWidth: "1px",
      },
    "&:hover fieldset": {
      borderColor: `${highlightColor} !important`,
      borderWidth: "2px !important",
    },
    //".Mui-disabled": {},
  };
};
