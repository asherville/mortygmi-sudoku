import React, { useState } from "react";
import { Paper, IconButton, Backdrop, CircularProgress } from "@mui/material";
import { Formik, Field, Form } from "formik";
import { TextField } from "formik-mui";
import Fingerprint from "@mui/icons-material/Fingerprint";
import axios from "axios";

function FakeAuth(props) {
  const { changeAuth } = props;
  const [isFetching, setIsFetching] = useState(false);

  const handleSubmit = (values, actions) => {
    const { pass, username } = values;
    if (!pass || !username) {
      actions.setSubmitting(false);
      return;
    }
    setIsFetching(true);
    return new Promise((resolve, reject) =>
      setTimeout(async () => {
        try {
          const response = await axios({
            method: "post",
            url: "http://localhost:5139/check_answer",
            data: {
              pass,
              username,
            },
          });
          if (response.status === 200) {
            resolve(response.data.correct);
          } else reject();
        } catch (err) {
          reject(err);
        }
      }, 500)
    )
      .then((result) => {
        actions.setSubmitting(false);
        if (result) {
          changeAuth();
        } else {
          actions.resetForm();
          actions.setErrors({ username: "Nope", pass: "Try again" });
          setIsFetching(false);
        }
      })
      .catch((err) => {
        console.log(err, "err");
        setIsFetching(false);
      });
  };

  return (
    <Paper className="auth-container" elevation={24}>
      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={isFetching}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      <Formik
        onSubmit={handleSubmit}
        initialValues={{
          username: "",
          pass: "",
        }}
      >
        {(props) => (
          <Form>
            <Field
              component={TextField}
              label={props.errors.username || "Login"}
              name="username"
              error={!!props.errors.username}
              variant="standard"
              fullWidth
              autoComplete="off"
              margin="normal"
              sx={{ mt: 0 }}
            />
            <Field
              component={TextField}
              label={props.errors.pass || "Password"}
              name="pass"
              type="password"
              error={!!props.errors.pass}
              variant="standard"
              fullWidth
              autoComplete="off"
              margin="normal"
            />
            <IconButton
              aria-label="fingerprint"
              variant="contained"
              sx={{ mt: 2 }}
              type="submit"
            >
              <Fingerprint fontSize="large" />
            </IconButton>
          </Form>
        )}
      </Formik>
    </Paper>
  );
}

export default FakeAuth;
