const settings = require("config");
const express = require("express");
const bodyParser = require("body-parser");
const compression = require("compression");
const path = require("path");

const app = express();
const cors = require("cors");

app.use(compression());
app.use(bodyParser.json({ limit: settings.bodyLimit }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.use(express.static(path.join(__dirname, "public")));

const PORT = settings.port;

const validateParam = (param) => param && typeof param === "string";
app.get("/getSudokuData", (_, res) => {
  // ** TO DO:
  // change stupid path logic to [i][j] directions
  const response = {};
  const metaObj = {};

  const initialAnswer = [
    8, 0, 0, 0, 0, 3, 0, 7, 0, 0, 0, 0, 6, 0, 0, 0, 9, 0, 0, 0, 0, 0, 0, 0, 2,
    0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 4, 5, 1, 0, 0, 0, 0, 0, 7, 0,
    0, 0, 3, 0, 0, 0, 1, 0, 0, 8, 0, 9, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 6, 8,
    0, 1, 0, 4, 0, 0,
  ].map((value) => value || "");
  const resultedAnswer =
    "812943675753682491649175283154369287237845169896721534521438796974526318368917452";

  const road = [
    2, 3, 10, 11, 12, 19, 20, 21, 4, 5, 14, 15, 22, 23, 24, 7, 9, 16, 18, 26,
    27, 28, 30, 37, 38, 46, 47, 48, 31, 32, 33, 40, 50, 51, 34, 35, 36, 44, 45,
    52, 54, 55, 56, 64, 65, 66, 73, 58, 59, 68, 69, 76, 78, 61, 63, 70, 71, 72,
    80, 81,
  ];
  const fullRoad = [
    1, 2, 3, 10, 11, 12, 19, 20, 21, 4, 5, 6, 13, 14, 15, 22, 23, 24, 7, 8, 9,
    16, 17, 18, 25, 26, 27, 28, 29, 30, 37, 38, 39, 46, 47, 48, 31, 32, 33, 40,
    41, 42, 49, 50, 51, 34, 35, 36, 43, 44, 45, 52, 53, 54, 55, 56, 57, 64, 65,
    66, 73, 74, 75, 58, 59, 60, 67, 68, 69, 76, 77, 78, 61, 62, 63, 70, 71, 72,
    79, 80, 81,
  ];
  let i = 0;

  const message = `Bruteforcedorgoogled?FuArtoinkaLaspent3mtryingmakeitperfect!let’sseeyournextstep!`;
  const afterChange = [];

  while (i < 81) {
    metaObj[i + 1] = {
      value: resultedAnswer[i],
      disabled: !!initialAnswer[i],
    };
    const value = message[i].toUpperCase();
    const index = fullRoad[i];
    afterChange[index - 1] = value;
    i++;
  }
  response.metaObj = metaObj;
  response.initialAnswer = initialAnswer;
  response.resultedAnswer = resultedAnswer;
  response.path = road;
  response.fullPath = fullRoad;

  response.afterChange = afterChange;

  res.status(200).send(response);
});
app.post("/check_answer", (req, res) => {
  try {
    const { pass, username } = req.body;
    if (!validateParam(pass) || !validateParam(username)) {
      return res.status(400).send({ correct: false });
    }
    const formatted_pass = pass.toLowerCase().trim();
    const formatted_username = username.toLowerCase().trim();
    return res.status(200).send({
      correct: formatted_pass === "password" && formatted_username === "login",
    });
  } catch (err) {
    console.log(err, "err");
  }
});

app.listen(PORT, () => {
  console.log(`Listening at ${PORT}`);
});
