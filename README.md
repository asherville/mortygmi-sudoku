# MortyGMI Sudoku

## Disclaimer
My friend asked me to do this and it has never meant to scale. No logic for different environments, all was written in couple of evenings.

## Description
Some riddle website with fake login and sudoku with autoplayed music on solve. Can be found here: https://morty-gmi.com/
Credentials for fake authorization: login is 'login' and password is 'password'. 

## Installation
You'll need node, npm, react-app-rewired, react-scripts and maybe something else I forgot. Simple npm i + npm start (or npm run server) will make it run. (3006 and 5139 ports). Not gonna help you with deploy. 

## Project status
Done and forgotten.

